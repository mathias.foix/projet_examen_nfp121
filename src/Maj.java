import java.util.Iterator;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private Donnees donnees;
	
	@Override
    public void traiter(Position position, double valeur) {
        if (position == null) {
            return;
        }
        
        if(!this.donnees.getData().containsKey(position)) {
            this.donnees.traiter(position, valeur);
        }
        
        super.traiter(position, valeur);
    }
    
    @Override
    public void gererDebutLotLocal(String nomLot) {
        this.donnees = new Donnees();
        this.donnees.gererDebutLot(nomLot);
    }
    
    @Override
    public void gererFinLotLocal(String nomLot) {        
        System.out.println("Position mis � jour pour le lot " + nomLot + ": ");
        for (int i = 0; i < this.donnees.getData().size(); i++) {
        	System.out.println("Position: " + this.donnees.getData().get(i));
        }
    }
}
