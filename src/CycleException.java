public class CycleException extends RuntimeException {
	public CycleException() {
		super("Le traitement est d�j� pr�sent dans les traitements � venir");
	}
	
	public CycleException(String message) {
		super(message);
	}
}
