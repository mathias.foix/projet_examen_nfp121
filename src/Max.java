/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double max = Double.NEGATIVE_INFINITY;
	
	public double max() {
		return this.max;
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if (this.max < valeur) {
			this.max = valeur;
		}
		
		super.traiter(position, valeur);
		}
}
