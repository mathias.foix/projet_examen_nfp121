/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " + this);
	}
	
	
	
	/* 
	 * On v�rifie si les points ont les m�mes valeurs d�attributs
	 * Car les points sont diff�rents m�mes si ils ont les m�mes valeurs d'abcisse et d'ordonn�e
	 */
	@Override 
    public boolean equals(Object obj) {  
		//Si obj n'est pas une intance de la classe Position 
		//return false
		if (!(obj instanceof Position)) {
				return false;
		}
		//Si obj est pas une intance de la classe Position 
		//return true et la variable prend les valeurs de Position 
        if (obj instanceof Position) { 
        	Position other = (Position) obj; 
        	return (this.x == other.x) && (this.y == other.y);
        }
		return false; 
    } 
	
	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}
	
}
