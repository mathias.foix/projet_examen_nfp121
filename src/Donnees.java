import java.util.HashMap;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	//Cr�ation de la HasMap
	private HashMap <Position, Double> data; 
	
	//Insertion des donn�es dans la HashMap
	public Donnees() { 
		this.data = new HashMap <Position, Double> ();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.data.put(position, valeur);
		super.traiter(position, valeur);
		}
	public  HashMap <Position, Double> getData() {
		return this.data;
	}
}
