import java.util.*;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Positions extends PositionsAbstrait {
	
	private ArrayList<Position> positions =  new ArrayList<Position>();

	
	/*
	 * La variable nombre r�cup�re la taille de la liste puis l'affiche  
	 */
	@Override
	public int nombre() {
		int nombre = this.positions.size();
		System.out.println("La taille de ma liste est = " + nombre);
		return nombre;
	}
	
	/*
	 * On r�cup�re la m�thode fr�quence dans la classe m�re Collections 
	 * Pour compter le nombre de fois qu'une position apparait 
	 */
	@Override
	public int frequence(Position position) {
		int frequence = Collections.frequency(this.positions,  position);
		return frequence;
	}
	
	/*
	 * Affiche toutes les positions enregistr�es 
	 */
	@Override 
	public void traiter(Position position, double valeur) {
		this.positions.add(position);
		super.traiter(position, valeur);
		System.out.println(this.positions);
	}
	
	
	/*
	 * Affiche la position de la liste li� � l'indice 
	 */
	@Override
	public Position position(int indice) {
		Position positionARetourner;
		positionARetourner = this.positions.get(indice);
		System.out.println(positionARetourner);
		return positionARetourner;
	}

}
